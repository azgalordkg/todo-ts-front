import {ActionTypes, ADD_TASK, DELETE_TASK, FETCH_LIST, MODAL, PUT_TASK} from "../actions/list.action";
import {ListItemType} from "../components/List/List";
import {FULFILLED, PENDING, REJECTED} from "../actions/promise.action";

const initialState = {
  list: null as Array<ListItemType> | null,
  error: null as any | null,
  loading: false,
  modalVisible: false
};

type InitialState = typeof initialState;

export function ListReducer(state = initialState, action: ActionTypes): InitialState {
  switch (action.type) {
    case MODAL:
      return { ...state, modalVisible: action.payload };
  
    case `${FETCH_LIST}${FULFILLED}`:
      return { ...state, list: action.payload, loading: false };
    case `${FETCH_LIST}${PENDING}`:
      return { ...state, loading: true };
    case `${FETCH_LIST}${REJECTED}`:
      return { ...state, error: action.payload, loading: false };
      
    case `${ADD_TASK}${FULFILLED}`:
      return { ...state, loading: false };
    case `${ADD_TASK}${PENDING}`:
      return { ...state, loading: true };
    case `${ADD_TASK}${REJECTED}`:
      return { ...state, error: action.payload, loading: false };
      
    case `${DELETE_TASK}${FULFILLED}`:
      return { ...state, loading: false };
    case `${DELETE_TASK}${PENDING}`:
      return { ...state, loading: true };
    case `${DELETE_TASK}${REJECTED}`:
      return { ...state, error: action.payload, loading: false };
      
    case `${PUT_TASK}${FULFILLED}`:
      return { ...state, loading: false };
    case `${PUT_TASK}${PENDING}`:
      return { ...state, loading: true };
    case `${PUT_TASK}${REJECTED}`:
      return { ...state, error: action.payload, loading: false };
      
    default:
      return state;
  }
}

export default ListReducer;
