import { combineReducers } from "redux";
// local files
import { ListReducer } from "./list.reducer";

export const reducer = combineReducers({
  tasks: ListReducer
});

type RootReducer = typeof reducer;

export type AppState = ReturnType<RootReducer>;
