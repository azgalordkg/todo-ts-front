import React from "react";
import { useSelector, useDispatch } from "react-redux";
// local files
import "./App.css";
import { Header } from "../../components/Header/Header";
import { listSelector } from "../../store/selectors";
import {addTask, deleteTask, fetchList, modalToggle, putTask} from "../../actions/list.action";
import {ListComponent, ListItemType} from "../../components/List/List";
import { ModalComponent } from "../../components/Modal/Modal";
import { Form, message } from "antd";

export type InitialValuesType = {
  title: string
  description: string
  status: boolean | undefined
  _id: string | undefined
}

const defaultInitialValies = {
  title: "",
  description: "",
  status: false as boolean | undefined,
  _id: undefined as string | undefined
};

function App() {
  const tasks = useSelector(listSelector);
  const dispatch = useDispatch();
  const { loading, list, modalVisible } = tasks;
  const [ form ] = Form.useForm();
  const [ initialValues, setInitialValues ] = React.useState(defaultInitialValies);
  const [ isTaskEdit, setIsTaskEdit ] = React.useState(false);
  
  React.useEffect(() => {
    dispatch(fetchList());
  }, [dispatch]);
  
  const addTaskClicked = () => {
    setInitialValues(defaultInitialValies);
    dispatch(modalToggle(true));
  };
  
  const onFinish = (values: any, id: string | undefined, status: boolean | undefined) => {
    const submitValues = { ...values };
    form.resetFields();
    
    if (isTaskEdit) {
      submitValues.status = status;
      submitValues._id = id;
      dispatch(putTask(id, submitValues));
    } else {
      submitValues.status = false;
      dispatch(addTask(submitValues));
    }
  };
  
  const confirm = (e: any, id: string | undefined) => {
    message.success('Deleting confirmed');
    if (id) {
      dispatch(deleteTask(id));
    }
  };
  
  const onCheckboxClick = (item: ListItemType) => {
    const { status, _id } = item;
    const task = {
      ...item,
      status: !status,
      __v: undefined
    };
    
    dispatch(putTask(_id, task));
  };
  
  const cancel = () => {
    message.error('Deleting canceled');
  };
  
  const onEditClick = (item: ListItemType) => {
    const { title, description, status, _id } = item;
  
    setIsTaskEdit(true);
    setInitialValues({
      title,
      description,
      status,
      _id
    });
    dispatch(modalToggle(true));
  };
  
  const onModalCancelClick = () => {
    setIsTaskEdit(false);
    dispatch(modalToggle(false))
  };
  
  return (
    <div className="App">
      <Header addTaskClicked={addTaskClicked} />
      <ListComponent
        list={list}
        loading={loading}
        deleteConfirm={confirm}
        cancel={cancel}
        onCheckboxClick={onCheckboxClick}
        onEditClick={onEditClick}
      />
      <ModalComponent
        modalVisible={modalVisible}
        onCancelClick={onModalCancelClick}
        initialValues={initialValues}
        onFinish={onFinish}
        loading={loading}
        form={form}
        isTaskEdit={isTaskEdit}
      />
    </div>
  );
}

export default App;
