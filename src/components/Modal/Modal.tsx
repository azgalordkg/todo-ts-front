import React from "react";
import { Button, Form, Input, Modal } from "antd";
import { InitialValuesType } from "../../containers/App/App";
import { FormInstance } from "antd/es/form";

type PropsType = {
  modalVisible: boolean
  onCancelClick: () => void
  initialValues: InitialValuesType
  onFinish: (values: any, id: string | undefined, status: boolean | undefined) => void
  loading: boolean
  form: FormInstance
  isTaskEdit: boolean
}

export const ModalComponent: React.FC<PropsType> = React.memo((
  { modalVisible, onCancelClick, initialValues, onFinish, loading, form, isTaskEdit }
) => {
  form.setFieldsValue(initialValues);
  
  return (
    <Modal
      title="Add new task"
      visible={modalVisible}
      onCancel={onCancelClick}
      footer={[
        <Button key="back" onClick={onCancelClick}>
          Cancel
        </Button>
      ]}
    >
      <Form
        form={form}
        labelCol={{span: 4}}
        wrapperCol={{span: 20}}
        layout="horizontal"
        initialValues={initialValues}
        size="large"
        onFinish={(values) => onFinish(values, initialValues._id, initialValues.status)}
      >
        <Form.Item name="title" label="Title">
          <Input/>
        </Form.Item>
        <Form.Item name="description" label="Description">
          <Input.TextArea rows={4}/>
        </Form.Item>
        <Form.Item label=" " colon={false}>
          <Button
            style={{width: "150px"}}
            type="primary"
            htmlType="submit"
            loading={loading}
          >
            {`${isTaskEdit ? "Edit Task" : "Create Task"}`}
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  )
});
