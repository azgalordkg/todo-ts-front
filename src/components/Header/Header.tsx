import React from "react";
import { Button } from "antd";
import Logo from "../../assets/logo.png";
import "./Header.css";

type PropsType = {
  addTaskClicked: () => void
}

export const Header: React.FC<PropsType> = React.memo(({ addTaskClicked }) => {
  return (
    <header className="Header">
      <div className="HeaderLogo">
        <img src={Logo} alt=""/>
        <span>React TypeScript To Do List</span>
      </div>
      <Button
        type="primary"
        onClick={addTaskClicked}
      >
        Add new task
      </Button>
    </header>
  )
});
