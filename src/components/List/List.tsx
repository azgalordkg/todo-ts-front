import React from "react";
import { Button, List, Skeleton, Checkbox, Popconfirm } from "antd";
import "./List.css";

export type ListItemType = {
  _id?: string
  title: string
  description: string
  status?: boolean
  __v?: number
}

type PropsType = {
  list: null | Array<ListItemType>
  loading: boolean,
  deleteConfirm: (e: any, id: string | undefined) => void
  cancel: () => void
  onCheckboxClick: (item: ListItemType) => void
  onEditClick: (item: ListItemType) => void
}

export const ListComponent: React.FC<PropsType> = ({ list, loading, deleteConfirm, cancel, onCheckboxClick, onEditClick }) => {
  return (
    <div className="List">
      {list && list.length && <List
        className="demo-loadmore-list"
        itemLayout="horizontal"
        dataSource={list}
        loading={loading}
        renderItem={item => (
          <List.Item
            actions={[
              <Button type="dashed" onClick={() => onEditClick(item)}>Edit</Button>,
              <Popconfirm
                title="Are you sure delete this task?"
                onConfirm={(e) => deleteConfirm(e, item._id)}
                onCancel={cancel}
                okText="Yes"
                cancelText="No"
              >
                <Button danger>Delete</Button>
              </Popconfirm>
            ]}
          >
            <Skeleton loading={loading} avatar title={false} active>
              <Checkbox style={{ marginRight: "20px" }} onClick={() => onCheckboxClick(item)} checked={item.status} />
              <List.Item.Meta
                title={item.title}
                description={item.description}
              />
            </Skeleton>
          </List.Item>
        )}
      />}
    </div>
  )
};
