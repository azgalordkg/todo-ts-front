import axiosInstance from "../axios";
import {Dispatch} from "redux";
import {InferActionTypes} from "../store";
import {ListItemType} from "../components/List/List";

export const FETCH_LIST = "FETCH_LIST";
export const ADD_TASK = "ADD_TASK";
export const DELETE_TASK = "DELETE_TASK";
export const PUT_TASK = "PUT_TASK";
export const MODAL = "MODAL";

export type ActionTypes = InferActionTypes<typeof actions>;

type DispatchType = Dispatch<ActionTypes>;
// type ThunkType = ThunkAction<Promise<void>, AppState, unknown, ActionTypes>;

export const actions = {};

export const modalToggle = (status: boolean) => ({type: MODAL, payload: status});

export function fetchList() {
  return (dispatch: DispatchType) => {
    dispatch({
      type: FETCH_LIST,
      payload: axiosInstance
        .get("/tasks/")
        .then(res => res.data)
    });
  }
}

export function addTask(task: ListItemType) {
  return (dispatch: DispatchType) => {
    dispatch({
      type: ADD_TASK,
      payload: axiosInstance
        .post("/tasks/", task)
        .then(res => {
          dispatch(fetchList());
          dispatch(modalToggle(false));
          return res.data;
        })
    });
  }
}

export function deleteTask(taskId: string | undefined) {
  return (dispatch: DispatchType) => {
    dispatch({
      type: DELETE_TASK,
      payload: axiosInstance
        .delete(`/tasks/${taskId}`)
        .then(res => {
          dispatch(fetchList());
          return res.data;
        })
    });
  }
}

export function putTask(taskId: string | undefined, task: ListItemType) {
  return (dispatch: DispatchType) => {
    dispatch({
      type: PUT_TASK,
      payload: axiosInstance
        .put(`/tasks/${taskId}`, task)
        .then(res => {
          dispatch(fetchList());
          dispatch(modalToggle(false));
          return res.data;
        })
    })
  }
}
