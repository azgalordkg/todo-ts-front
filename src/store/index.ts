import { applyMiddleware, createStore } from "redux";
import ReduxThunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import promise from "redux-promise-middleware";
// local files
import { reducer } from "../reducers";

type PropertiesTypes<T> = T extends {[key: string]: infer U} ? U : never;
export type InferActionTypes<T extends {[key: string]: (...keys: any[]) => any}> = ReturnType<PropertiesTypes<T>>;

const middleware = composeWithDevTools(applyMiddleware(ReduxThunk, promise));
const store = createStore(reducer, middleware);

export default store;
