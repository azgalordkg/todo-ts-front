import {AppState} from "../reducers";

export const listSelector = (state: AppState) => state.tasks;
